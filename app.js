var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

const passport = require('./config/passport');
const session = require('express-session');

var Usuario = require('./models/Usuario');
var Token = require('./models/Token');

const store = new session.MemoryStore;

// Conexión a mongodb mediante mongoose
mongoose.connect('mongodb://localhost/red_bicicletas', {useUnifiedTopology: true, useNewUrlParser: true});

mongoose.Promise = global.Promise;

var db = mongoose.connection;

// Vincular la conexión de un evento de error (para obtener 
// notificiaciones de errores en la conexión a la BD)
db.on("error", console.error.bind('Error de conexión con MongoDB'));

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

// ########## ROUTERS #####################

// Importo el router de las bicicletas
var bicicletasRouter = require('./routes/bicicletas');

// Importo el router de los usuarios
var usuariosRouter = require('./routes/usuarios');

// Importo el router de los tokens
var tokenRouter = require('./routes/tokens');

// #######################################

// ############## API ROUTERS #######################

// Importo el router de la autenticación por API
var authAPIRouter = require('./routes/api/auth');

// Importo el router de la API de las bicicletas
var bicicletasAPIRouter = require('./routes/api/bicicletas');

// Importo el router de la API de los usuarios
var usuariosAPIRouter = require('./routes/api/usuarios');

// Importo el router de la API de las reservas
var reservasAPIRouter = require('./routes/api/reservas');

// ##################################################


var app = express();

app.use(session({

  cookie: {magAge: 240*60*60*1000}, //Tiempo en milisegundos

  store: store,

  saveUninitialized: true,


  resave: "true",


  secret: "cualquier cosa no pasa nada 477447"

}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Secret key
app.set('secretKey', 'JWT_PDW_!!223344');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Usamos passport
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
/* app.use('/users', usersRouter); */

// ###### RUTAS SESIONES ######

// Vista del login
app.get('/login', function(req, res) {

  res.render("session/login");

});

// Envío del formulario del login
app.post('/login', function(req, res, next) {

  passport.authenticate("local", function (err, usuario, info) {

    if(err) return next(err);

    if (!usuario) return res.render("session/login", {info});

    req.logIn(usuario, function(err) {

      if(err) return next(err);

      return res.redirect('/');

    });

  })(req, res, next);

});

// Redirección del logout
app.get('/logout', function (req, res) {

  req.logOut(); // Limpiamos la sesión

  res.redirect('/');

});

// Vista del formulario de recuperación password
app.get('/forgotPassword', function(req, res) {

  res.render('session/forgotPassword');

});

// Envío del formulario de recuperación password
app.post('/forgotPassword', function(req, res) {

  Usuario.findOne({email: req.body.email}, function(err, usuario) {

    if(!usuario) return res.render('session/forgotPassword', {info: {message: "No existe ese email en nuestra BBDD."}});


    usuario.resetPassword(function(err) {

      if(err) return next(err);

      console.log("session/forgotPasswordMessage");

    });

    res.render('session/forgotPasswordMessage');

  });

});

app.get('/resetPassword/:token', function (req, res, next) {

  Token.findOne({token: req.params.token}, function(err, token) {

    if(!token) return res.status(400).send({type: "not-verified", msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado."});

    Usuario.findById(token._userId, function(err, usuario) {

      if (!usuario) return res.status(400).send({msg: "No existe un usuario asociado al token."});

      res.render("session/resetPassword", {errors: {}, usuario: usuario});

    });

  });

});

app.post('/resetPassword', function(req, res) {

  if(req.body.password != req.body.confirm_password) {

    res.render("session/resetPassword", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, 
    
    usuario: new Usuario({email: req.body.email})});

    return;

  }

  Usuario.findOne({email: req.body.email}, function(err, usuario) {

    usuario.password = req.body.password;

    usuario.save(function(err) {

      if(err) {

        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});

      }else {

        res.redirect('/login');

      }

    });

  });

});

function validarUsuario(req, res, next) {

  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {

    if(err) {

      res.json({status:'error', message: err.message, data: null});

    }else {

      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);

      next();

    }

  });

}


// Uso el router importado
app.use('/bicicletas', loggedIn, bicicletasRouter);

// Uso el router de los tokens
app.use('/token', tokenRouter);

// Uso el router de los usuarios
app.use('/usuarios', loggedIn, usuariosRouter);

// Uso el router de la autenticación por API
app.use('/api/auth', authAPIRouter);

// Uso el router de la API de bicicletas
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);

// Uso el router de la API de usuarios
app.use('/api/usuarios', usuariosAPIRouter);

// Uso el router de la API de reservas
app.use('/api/reservas', reservasAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {

  if(req.user) {

    next();

  }else {

    console.log("Usuario no logueado");

    res.redirect('/login');

  }

}

module.exports = app;
