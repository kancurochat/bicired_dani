let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({

    bicicletaID: Number,

    color: String,

    modelo: String,

    ubicacion: { type: [Number], index: true }

});

let Bicicleta = function (id, color, modelo, ubicacion) {
    
    this.id = id;
    
    this.color = color;

    this.modelo = modelo;

    this.ubicacion = ubicacion;

}

// Obtiene un documento con todas las bicis
bicicletaSchema.statics.allBicis = function (cb)  {

    return this.find({}, cb);

};

// Añade una bici a la colección de bicis
bicicletaSchema.statics.add = function (aBici, cb) {

    return this.create(aBici, cb);

};

// Encuentra una bici en la colección mediante el ID
bicicletaSchema.statics.findById = function (id, cb) {

    // Cambié la función de mongoose de findById a findOne
    // debido a que no conseguía que me funcionase el get del update
    // usándola
    return this.findOne(id, cb);

};

// Elimina una bici de la colección mediante el ID
bicicletaSchema.statics.removeById = function (id, cb) {

    return this.findByIdAndDelete(id, cb);

};




bicicletaSchema.statics.update = function (idDocumento, documento, cb) {

   /*  let consulta = {
        _id: id,
        bicicletaID: idBici,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    };

    this.updateOne(consulta, cb); */

    return this.findByIdAndUpdate(idDocumento, documento, cb);

};



module.exports = mongoose.model("Bicicleta", bicicletaSchema);