const moment = require('moment');
let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let reservaSchema = new Schema ({

    desde: Date,

    hasta: Date,

    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta"},

    usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}

});

let Reserva = function(desde, hasta, bicicleta, usuario) {

    this.desde = desde;

    this.hasta = hasta;

    this.bicicleta = bicicleta;

    this.usuario = usuario;

};

// Cuántos días está reservada la bicicleta
reservaSchema.methods.diasDeReserva = function() {

    return moment(this.hasta.diff(moment(this.desde), "days") + 1);

};

reservaSchema.statics.reservas = function(cb) {

    return this.find({}, cb);

}

module.exports = mongoose.model("Reserva", reservaSchema);