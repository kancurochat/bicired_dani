let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function(req, res) {

    Bicicleta.allBicis(function (err, bicis) {

        if (err) res.status(500).send(err.message);

        res.status(200).json({
            bicicletas: bicis
        });

    });

};

exports.bicicleta_create = function(req, res) {
    
    let bici = new Bicicleta({

        bicicletaID: req.body.bicicletaID,

        color: req.body.color,

        modelo: req.body.modelo,

        ubicacion: req.body.ubicacion

    });

    Bicicleta.add(bici, function (err, bici) {

        if (err) res.status(500).send(err.message);

        res.status(201).json({
            bicicletas: bici
        });

    });

};

exports.bicicleta_delete = function(req, res) {
    
    Bicicleta.removeById(req.body._id, function (err, result) {

        if (err) res.status(500).send(err.message);

        res.status(200).send();

    });

}

exports.bicicleta_update = function(req, res) {

    /* Bicicleta.update(req.body._id, req.body.bicicletaID, req.body.color, req.body.modelo, req.body.ubicacion, 
        function (err, result) {

            if (err) res.status(500).send(err.message);

            res.status(201).send();

        }); */

        let idDocumento = req.body._id;

        Bicicleta.update(idDocumento, req.body, function (err, result) {

            if (err) res.status(500).send(err.message);

            res.status(201).json(
                req.body
            );

        });

}