let Usuario = require("../../models/Usuario");

exports.usuarios_reservar = function (req, res) {


    // Preguntar si es necesario definir la función como statics en el modelo
    Usuario.findById(req.body._id, function(err, usuario) {

        if(err) res.status(500).send(err.message);

        console.log(usuario);

        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err) {

            if(err) res.status(500).send(err.message);

            console.log("¡Reservada!");

            res.status(200).send();

        });

    });

};