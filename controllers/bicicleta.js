let Bicicleta = require("../models/Bicicleta");

// Obtiene la vista con la tabla de bicicletas
exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, result) {

        if (err) res.status(500).send(err.message);

        res.render("bicicletas/index", {bicis: result});

    });
}

// Obtiene el formulario de inserción de nuevas bicicletas
exports.bicicleta_create_get = function (req, res) {
    res.render("bicicletas/create");
}

// Crea un objeto de la clase Bicicleta, recogiendo las propiedades por el método POST
// y la añade a la array de bicicletas
exports.bicicleta_create_post = function (req, res) {
    let bici = new Bicicleta({

        bicicletaID: req.body.bicicletaID,

        color: req.body.color,

        modelo: req.body.modelo,

        ubicacion: [req.body.latitud, req.body.longitud]

    });

    Bicicleta.add(bici, function(err, bici) {

        if(err) res.status(500).send(err.message);


        // Redirección al index siguiendo el patrón de diseño PRG(POST-REDIRECT-GET)
        res.status(201).redirect("/bicicletas");

    });

    
}

// Obtiene el id de la bicicleta escogida y la borra del array
exports.bicicleta_delete_post = function (req, res) {

    Bicicleta.removeById(req.body.id, function (err, result) {

        if (err) res.status(500).send(err.message);

        res.redirect("/bicicletas");

    });

    

}

// Obtiene el id de la bicicleta escogida y muestra la vista de actualizaciones
exports.bicicleta_update_get = function (req, res) {
    
    Bicicleta.findById({_id: req.params.id}, function(err, result) {

        if(err) res.status(500).send(err.message);

        res.render("bicicletas/update", {bici: result});
    });

}

exports.bicicleta_update_post = function (req, res) {

    let consulta = req.params.id;

    console.log(req.body);

    Bicicleta.update(consulta, req.body, function(err, result) {

        if(err) res.status(500).send(err.message);

        res.redirect("/bicicletas");

    });

}