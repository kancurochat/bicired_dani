let Usuario = require("../models/Usuario");

module.exports = {
    list: function(req,res,next){     
     Usuario.find({}, function(err,usuarios) { //Llama a la función find de la api de mongoose, pasándole como callback el render de la vista que muestra
            if(err) res.send(500, err.message); // la tabla de usuarios registrados
            res.render("usuarios/index", {usuarios: usuarios});
        });
    },
    update_get: function(req,res, next) {
        Usuario.findById(req.params.id, function (err, usuario) { //Llama a la función findById y le pasa como callback la función que renderiza
            res.render("usuarios/update", {errors:{}, usuario: usuario}); // la vista junto con el usuario seleccionado
        });
    },
    update: function(req, res, next){
        let update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario) { // Llama a la función de mongoose pasándole como callback 
            if (err) {                                                                   // una función que vuelve a renderizar la vista update si hay errores
                console.log(err);                                                        // y si no los hay hace un redirect a la tabla de usuarios
                res.render("usuario/update", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else {
                res.redirect("/usuarios");
                return;
            }
        });
    },
    create_get: function(req, res, next){
        res.render("usuarios/create", {errors:{}, usuario: new Usuario()}); // Renderiza la vista para crear un usuario nuevo
    },
    create: function(req, res, next) {
        if (req.body.password != req.body.confirm_password) {   // Si las contraseñas no coindicen, se renderiza de nuevo la vista de creación de usuarios
            res.render("usuarios/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }
        Usuario.create({nombre:req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsario) { // Si coinciden, se crea un usuario nuevo
            if(err){
                res.render("usuarios/create", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})}); // Si falla la creación, se renderiza otra vez la vista de creación
            } else {
                nuevoUsario.enviar_email_bienvenida(); //Si no, se manda el correo de bienvenida y se redirige a la tabla de usuarios.
                res.redirect("/usuarios");
            }
        });
    },
    delete: function(req,res,next){
        Usuario.findByIdAndDelete(req.body.id, function(err){ // Encuentra un usuario mediante id y lo elimina, si no falla, se redirige a la tabla de usuarios.
            if(err)
                next(err);
            else   
                res.redirect("/usuarios");
        });
    }
};
