let express = require('express');

let router = express.Router();

let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");

router.post("/reservar", usuariosControllerAPI.usuarios_reservar);

module.exports = router;