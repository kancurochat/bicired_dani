let express = require('express');

let router = express.Router();

let reservasControllerAPI = require('../../controllers/api/reservasControllerAPI');

router.get('/reservas', reservasControllerAPI.reservas);

module.exports = router;